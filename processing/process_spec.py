#!/usr/bin/env python3
import os
import tqdm
import re
import sys
import multiprocessing

#folder_name="modified"
#folder_name="spec3"
folder_name="spec-pause-ratio"
cpu_time_regex=None
max_jop_regex=re.compile("max-jOPS = ([0-9]*)");
critical_jop_regex=re.compile("critical-jOPS = ([0-9]*)");

def mean_std(in_list):
    mean = sum(in_list)/len(in_list)
    std = (sum([((x-mean)**2) for x in in_list])/len(in_list))**.5
    return (mean,std)

def process_folder(my_input):
    dirName, subdirList, fileList = my_input
    count = 0
    critical_jop_count = 0
    allocation_stall_count=0
    total_cpu_time = 0
    z_cpu_time=0
    avg_bytes = None
    max_bytes= None

    att_list=(lambda x: x[x.index(folder_name)+1:-1] )(dirName.split("/"))


    with open(f"{dirName}/jvm0.out") as f:
        tmp_file_in=f.read()
        count += (lambda x : 0 if not x or x.group(1)=="" else int(x.group(1)))(max_jop_regex.search(tmp_file_in));
        critical_jop_count += (lambda x : 0 if not x or x.group(1)=="" else int(x.group(1)))(critical_jop_regex.search(tmp_file_in));

    #with open(f"{dirName}/jvm0.log") as f:
    #    tmp_file_in=f.read()
        allocation_stall_count+=tmp_file_in.count("Allocation Stall")
        cpu_time_result = [ sum(x) for x in list(map(list,zip(*[(float(i[1]),float(i[1]) if i[0] =="Z" or i[0] =="G" else 0.0) for i in cpu_time_regex.findall(tmp_file_in.split("JNI global refs:")[0])])))]
        if len(cpu_time_result)>=2:
            if(cpu_time_result[1]==0.0):
                print(att_list)
            total_cpu_time += cpu_time_result[0]
            z_cpu_time+=cpu_time_result[1]
    with open(f"{dirName}/memory-usage.txt") as f:
        memory_file_tmp=f.read()
        avg_bytes=float(memory_file_tmp.split("\n")[8].split(":")[1])
        avg_bytes+=float(memory_file_tmp.split("\n")[1].split(":")[1])
        max_bytes=float(memory_file_tmp.split("\n")[7].split(":")[1])
        max_bytes+=float(memory_file_tmp.split("\n")[0].split(":")[1])
    return ("/".join(att_list),(count,avg_bytes,allocation_stall_count,total_cpu_time,z_cpu_time,max_bytes,"fail.txt" in fileList,critical_jop_count))


def main():
    global cpu_time_regex
    cpu_time_regex = re.compile(r"^\"([a-zA-Z])[a-zA-Z_# :0-9]*\".* os_prio=[0-9]* cpu=([0-9.]*)ms",re.MULTILINE)
    data = dict()
    with multiprocessing.Pool(16) as p:
        tmp_directory_list=[i for i in os.walk(f"/home/jmalloy1/results/{folder_name}/") if len(i[2])!=0]
        for iterator in tqdm.tqdm(p.imap(process_folder,tmp_directory_list),total=len(tmp_directory_list)):
            if(iterator[0] not in data):
                data[iterator[0]] = list()
            data[iterator[0]].append(iterator[1])
    out_string = "processes,gc,pre_iteration_gc,memory_mode,memory_config,avg_bytes_used,iterations,allocation_stalls,count,avg_bytes_used_std,iterations_std,allocation_stalls_std,total_cpu_time_mean,total_cpu_time_std,gc_cpu_time_mean,gc_cpu_time_std,max_bytes_mean,max_bytes_std,failed,critical_jops,critical_jops_std\n"
    for key, value in data.items():
        attrs=key.split("/")
        if len(attrs)<6:
            print(attrs)
        perf_mean, perf_std= mean_std([x[0]for x in value])
        crit_mean, crit_std= mean_std([x[7]for x in value])
        mem_mean, mem_std= mean_std([x[1]for x in value])
        allocation_stall_mean, allocation_stall_std = mean_std([x[2] for x in value])

        total_cpu_time_mean, total_cpu_time_std = mean_std([x[3] for x in value])
        z_cpu_time_mean, z_cpu_time_std = mean_std([x[4] for x in value])
        max_bytes_mean, max_bytes_std = mean_std([x[5] for x in value])
        failed="False"
        for x in value:
            if x[6]==True:
                failed="True"
                break
        print(key)
        out_string+=f"{attrs[0]},{attrs[1]},{attrs[2]},{attrs[3]},{attrs[4]},{mem_mean},{perf_mean},{allocation_stall_mean},{len(value)},{mem_std},{perf_std},{allocation_stall_std},{total_cpu_time_mean},{total_cpu_time_std},{z_cpu_time_mean},{z_cpu_time_std},{max_bytes_mean},{max_bytes_std},{failed},{crit_mean},{crit_std}\n"
        #    16/zgc/gc/xmx/fop/1024

    with open("out_data.csv","w") as out_file:
        out_file.write(out_string)





if __name__=="__main__":
    main()
