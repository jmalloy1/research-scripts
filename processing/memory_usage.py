#!/usr/bin/env python3
import os



def main():
    data = dict()
    for dirName, subdirList, fileList in os.walk("gc"):
        if("meminfo.txt" not in fileList):
            continue
        if("default" not in dirName):
            continue
        #print("/".join(dirName.split("/")[:-1]))
        tmp_string=""
        name = "/".join(dirName.split("/")[:-1])
        with open(f"{dirName}/meminfo.txt","r") as file:
            tmp_string = file.read()
        tmp_string = tmp_string.split("kB\n")[-1]
        memory_usage = [int(i) for i in tmp_string.split("\n")[1::2]]
        if(name not in data):
            data[name]=list()
        data[name].append(max(memory_usage))
        print(max(memory_usage))
        print("=================================================")
    out_string = "benchmark,mb_limit,avg_bytes,std_bytes\n"
    for key, value in data.items():
        mean = float(sum(value))/len(value)
        benchmark=key.split("/")[1]
        print(f"key: {key}, value_mean:{mean} value_std:{(sum([((x-mean)**2) for x in value])/len(value))**.5}")
        out_string+=f"{benchmark},default,{mean},{(sum([((x-mean)**2) for x in value])/len(value))**.5}\n"

    with open("default_memory_usage.csv","w") as out_file:
        out_file.write(out_string)





if __name__=="__main__":
    main()
