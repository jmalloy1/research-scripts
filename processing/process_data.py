#!/usr/bin/env python3
import os
import tqdm

def mean_std(in_list):
    mean = sum(in_list)/len(in_list)
    std = (sum([((x-mean)**2) for x in in_list])/len(in_list))**.5
    return (mean,std)

def main():
    data = dict()
    for dirName, subdirList, fileList in tqdm.tqdm(os.walk("5-23-22")):
        count = 0
        allocation_stall_count=0
        bytes = None

        att_list=dirName.split("/")[1:-1]
        if(len(fileList)==0):
            continue
        if("/".join(att_list) not in data):
            data["/".join(att_list)] = list()
        for i in fileList:
            if(i[:3]!="jvm"):
                continue
            with open(f"{dirName}/{i}") as f:
                tmp_file_in=f.read()
                count += tmp_file_in.count("completed")
                allocation_stall_count+=tmp_file_in.count("Allocation Stall")
        try:
            with open(f"{dirName}/memory-usage.txt") as f:
                bytes=int(f.read().split("\n")[0])
        except:
            bytes=0
        data["/".join(att_list)].append((count,bytes,allocation_stall_count))

    out_string = "benchmark,processes,gc,pre_iteration_gc,memory_mode,memory_config,bytes_used,iterations,allocation_stalls,count,bytes_used_std,iterations_std,allocation_stalls_std\n"
    for key, value in data.items():
        attrs=key.split("/")
        perf_mean, perf_std= mean_std([x[0]for x in value])
        mem_mean, mem_std= mean_std([x[1]for x in value])
        allocation_stall_mean, allocation_stall_std = mean_std([x[2] for x in value])
        out_string+=f"{attrs[4]},{attrs[0]},{attrs[1]},{attrs[2]},{'NA' if attrs[5]=='default' else attrs[3]},{attrs[5]},{mem_mean},{perf_mean},{allocation_stall_mean},{len(value)},{mem_std},{perf_std},{allocation_stall_std}\n"
        #    16/zgc/gc/xmx/fop/1024

    with open("out_data.csv","w") as out_file:
        out_file.write(out_string)





if __name__=="__main__":
    main()
