import subprocess
import os
import time
import sys
benchmarks=[
    #"avrora" ,
    #"batik" ,
    #"biojava" ,
    #"cassandra" ,
    #"eclipse" ,
    #"fop" ,
    #"graphchi" ,
    "h2" ,
    "jme" ,
    "jython" ,
    "kafka" ,
    "luindex" ,
    "lusearch" ,
    "pmd" ,
    "sunflow" ,
    "tomcat", 
    "tradebeans", 
    "tradesoap", 
    "xalan", 
    "zxing",
    #"h2o"
    ]
javaMemory=[
    ("","default"),
    #(" -XX:+DisableExplicitGC","disable_explicit"),
    #(" -Xmx1G","1G"),
    #(" -Xmx400M","400M"),
    #(" -Xms1G -Xmx1G","forced 1G"),
    #(" -Xms400M -Xmx400M","forced 400M")
    ]
javaCollectors=[
    #(" -XX:+UseShenandoahGC","Shenandoah"),
    #("","G1"),
    (" -XX:+UseG1GC -XX:-UseCompressedOops","G1-no compressed oop"),
    #(" -XX:+UseZGC","zgc"),
    #(" -XX:+UseParallelGC","parallel"),
    (" -XX:+UseParallelGC -XX:-UseCompressedOops","parallel-no compressed oop")
    ]

def getCommand(bench,memory,java,benchLocation,iterations,resultsDir,collector):
    return f"/usr/bin/time {java}{memory[0]} -Xlog:gc*:file=\"{resultsDir}/gc.log\"{collector[0]} -jar {benchLocation} {bench} -n{iterations}"

def main():

    if len(sys.argv)!=3:
        print(f"You must enter the two integer arguments for the number of invoctions and number of iterations of each test.For example:\npython3 {sys.argv[0]} 10 15\nfor ten invoctions with 15 iterations")
        exit()

    startTime = time.time()

    PROJ_DIR=os.getenv("PROJ_DIR")
    BENCHMARKS_DIR=os.getenv("BENCHMARKS_DIR")
    RESULTS_DIR=os.getenv("RESULTS_DIR")
    DACAPO_JAR=os.getenv("DACAPO_JAR")

    Java=f"{PROJ_DIR}/jdk/build/linux-x86_64-server-release/jdk/bin/java"
    if PROJ_DIR==None or BENCHMARKS_DIR==None or RESULTS_DIR==None or DACAPO_JAR==None:
        print("please set the environment variables.")
        exit()


    failed=str()
    for memory in javaMemory:
        for bench in benchmarks:
            for collector in javaCollectors:
                for invocs in range(int(sys.argv[1])):
                    result=RESULTS_DIR+"/"+collector[1]+"/"+bench+"/"+memory[1]+"/"+str(invocs)
                    os.makedirs(result,exist_ok=True)
                    print(f"{time.time()-startTime:.2f}-starting {bench} benchmark with {memory[1]} memory using the {collector[1]} collector #{invocs}")
                    with open(result+"/stdout.txt","wb+") as f:
                        out=subprocess.run([getCommand(bench,memory,Java,DACAPO_JAR,sys.argv[2],result,collector)],shell=True,stdout=f,stderr=f)
                    if(out.returncode==1):
                        print("fail")
                        failed+=f"{time.time()-startTime:.2f}-starting {bench} benchmark with {memory[1]} memory using the {collector[1]} collector #{invocs}\n"
    with open(f"{RESULTS_DIR}/failed.log","w+") as f:
        f.write(failed)
                        
                        
                        

if __name__=="__main__":
    main()
