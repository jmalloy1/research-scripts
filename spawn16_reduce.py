#!/usr/bin/python3
import subprocess
import os
import threading
import time
import signal
from pathlib import Path

#JAVA="/home/jmalloy1/git/jdk-reduce/build/linux-x86_64-server-release/jdk/bin/java"
JAVA="/home/jmalloy1/git/jdk/build/linux-x86_64-server-release/jdk/bin/java"
JAVA_FLAGS="-XX:+UseZGC"
RESULTS="/home/jmalloy1/results"
BENCHMARK="/home/jmalloy1/benchmarks/java/dacapo/my-dacapo.jar -c timeCallback jme -n1000"
CGROUP="jvm-cgroup"

pids=set()
threads=[]
should_continue=True

def thread(number,memory,location):
    global JAVA
    global JAVA_FLAGS
    global BENCHMARK
    global CGROUP
    global pids
    global should_continue
    count=0
    p =0
    if not os.path.isdir(f"./tmp-{number}"):
        os.mkdir(f"./tmp-{number}")
    while should_continue:
        with open(f"{location}/jvm{number}-{count}.txt","w") as file:
            p = subprocess.Popen(f"{JAVA} {JAVA_FLAGS} {memory} -jar {BENCHMARK}".split(" "),stderr=file,stdout=file,cwd=f"./tmp-{number}/")
            pids.add(p.pid)
            with open(f"/sys/fs/cgroup/{CGROUP}/cgroup.procs", "w") as proc_file:
                proc_file.write(f"{p.pid}")
            p.wait()
        count +=1

def main():
    global JAVA
    global JAVA_FLAGS
    global BENCHMARK
    global CGROUP
    global pids
    global threads
    global should_continue
    memory_list=[("64","-Xmx64m","8260907008"),("128","-Xmx128m","10959462400"),("256","-Xmx256m","15576219648"),("512","-Xmx512m","21489250304"),("1024","-Xmx1024m","30541955072"),("2048","-Xmx2048m","48303579136"),("4096","-Xmx4096m","50531790848"),("8192","-Xmx8192m","50417606656")]
    threads = []
    for memory in memory_list:
        for k in range(10):
            for iterator in memory_list:
                location=f"/home/jmalloy1/results/jme/{memory[0]}/{k}"
                Path(location).mkdir(parents=True,exist_ok=True)
                should_continue=1
                max_memory=0
                threads=[]
                pids=set()
                if not os.path.isdir(f"/sys/fs/cgroup/{CGROUP}"):
                    os.mkdir(f"/sys/fs/cgroup/{CGROUP}")
                with open(f"/sys/fs/cgroup/{CGROUP}/memory.max","w") as file:
                    file.write(memory[2])
                    file.flush()
                for i in range(16):
                    x=threading.Thread(target=thread, args=(i,iterator[1],location))
                    x.start()
                    threads.append(x)
                    time.sleep(60)
                with open(f"{location}/time.txt","w+") as time_file:
                    time_file.write(round(time.time() * 1000))
                time.sleep(600)
                should_continue=False
                for i in pids:
                    try:
                        os.kill(i,9)
                    except OSError as err:
                        print(f"process {i} does not exist")
                for i in threads:
                    i.join()

if __name__=="__main__":
    os.setpgrp()
    main()
