#!/usr/bin/bash
#JAVA="/home/jmalloy1/git/jdk-reduce/build/linux-x86_64-server-release/jdk/bin/java"
JAVA="/home/jmalloy1/git/jdk/build/linux-x86_64-server-release/jdk/bin/java"
JAVA_FLAGS="-XX:+UseZGC"
BENCHMARK="/home/jmalloy1/benchmarks/java/dacapo/dacapo-evaluation-git+309e1fa.jar"
CGROUP="jvm-cgroup"


OUTPUT_DIR="/home/jmalloy1/results"

pid_arr=()


#benchmarks=("jython" "kafka" "luindex" "lusearch" "pmd" "sunflow" "tomcat" "tradebeans" "tradesoap" "xalan" "zxing")
#benchmarks=("h2" "jme" "jython" "kafka" "luindex" "lusearch" "pmd" "sunflow" "tomcat" "tradebeans" "tradesoap" "xalan" "zxing")

benchmarks=("h2" "jme" "jython" "luindex" "lusearch" "pmd" "sunflow" "xalan" "zxing")

#memory_configs=("-Xmx1g" "-Xmx2g" "-Xmx4g" "-Xmx8g")
#memory_options=("-Xmx128m" "-Xmx256m" "-Xmx512m" "-Xms4g -Xmx4g" "-Xms8g -Xmx8g")
#memory_name=("Xmx128m" "Xmx256m" "Xmx512m" "set4g" "set8g")
memory_options=("-Xmx32m" "-Xmx64m")
memory_name=("Xmx32m" "Xmx64m")

do_gc_options=("" " --no-pre-iteration-gc")
gc_name=("gc" "no-gc")

for benchmark in ${benchmarks[@]}; do
    for do_gc in {0..1}; do
        for (( memory=0; memory<${#memory_options[@]}; memory++ )); do
            for iteration in {1..10}; do
                pid_arr=()
                mkdir -p ${OUTPUT_DIR}/${gc_name[$do_gc]}/${benchmark}/${memory_name[$memory]}/${iteration}
                for i in {1..16}; do
                    mkdir -p tmp${i}
                    cd tmp${i}
                    echo "starting ${benchmark}-${iteration}-${i}-${gc_name[$do_gc]}-${memory_name[$memory]}"
                    $JAVA $JAVA_FLAGS ${memory_options[$memory]} -jar $BENCHMARK ${benchmark}${do_gc_options[$do_gc]} -n1000 > ${OUTPUT_DIR}/${gc_name[$do_gc]}/${benchmark}/${memory_name[$memory]}/${iteration}/jvm${i}.txt 2>&1 &
                    pid_arr+=($!)
                    cd ..
                done
                printf "%s\n%s\n" $(date +%s) $(cat /proc/meminfo) >> ${OUTPUT_DIR}/${gc_name[$do_gc]}/${benchmark}/${memory_name[$memory]}/${iteration}/meminfo.txt
                sleep 30
                skip=0
                for i in ${pid_arr[@]}; do
                    if ! ps -p $i > /dev/null; then
                        skip=1
                    fi
                done

                if [[ $skip -eq 1 ]]; then
                    echo "fail" > ${OUTPUT_DIR}/${gc_name[$do_gc]}/${benchmark}/${memory_name[$memory]}/${iteration}/status.txt
                    for i in ${pid_arr[@]}; do
                        kill -9 ${i} 1>/dev/null 2>&1
                    done
                    wait
                    continue
                fi
                for i in {1..19};do
                    printf "%s\n%s\n" $(date +%s) $(cat /proc/meminfo) >> ${OUTPUT_DIR}/${gc_name[$do_gc]}/${benchmark}/${memory_name[$memory]}/${iteration}/meminfo.txt
                    sleep 30
                done
                for i in ${pid_arr[@]}; do
                    kill -9 ${i} 1>/dev/null 2>&1
                done
                wait
            done
        done
    done
done

rm -rf tmp*
