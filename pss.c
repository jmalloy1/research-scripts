#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <errno.h>


int total_pss_kb(uint32_t pid){
    int total;
    FILE *fp;
    char file_buff[25];
    char buffer[500];
    total =0;

    snprintf(file_buff,25,"/proc/%d/smaps",pid);
    fp=fopen(file_buff,"r");
    if(fp ==NULL){
        fprintf(stderr,"Failed to open %s\n%s\n",file_buff,strerror(errno));
        return -1;
    }


    while(fgets(buffer,500,fp)){
        if(strncmp(buffer,"Pss:",4)==0){
            total+=atoi(buffer+5);
        }
    }

    fclose(fp);

    return total;
}


int main(int argc, char **argv){
    int return_value;
    int pid;
    struct timeval tp;
    if(argc!=2){
        fprintf(stderr,"Incorrect number of arguments");
        exit(1);
    }
    pid = atoi(argv[1]);


    for(return_value=total_pss_kb(pid);return_value!=-1;return_value=total_pss_kb(pid)){
        gettimeofday(&tp,NULL);
        printf("%lld,%d\n",tp.tv_sec*1000+tp.tv_usec/1000,total_pss_kb(atoi(argv[1])));
    }

    usleep(500000);
    return 0;
}
