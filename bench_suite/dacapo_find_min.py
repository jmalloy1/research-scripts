#!/usr/bin/env python3
import vars

import time
import threading
import subprocess
import tqdm
import os
import math
import sys




def run(config):
    directory = config["directory"]
    lower = 2
    upper = 2
    memory_amount = None
    dacapo_location=config["dacapo_location"]


    if not os.path.isdir(f"/tmp/tmp-0"):
        os.mkdir(f"/tmp/tmp-0")

    while True:
        tqdm.tqdm.write(f"Walking up {upper}m")
        memory_flag = f"-Xmx{upper}m"
        gc_flag = vars.gc_map[config["gc"]]

        run_line = f"{config['jdk']}/build/linux-x86_64-server-release/jdk/bin/java {gc_flag} {memory_flag} -XX:+DisableExplicitGC -Xlog:gc -jar {dacapo_location} {config['dacapo_benchmark']} -n3"
        run_result = subprocess.run([x for x in run_line.split(" ") if x!="" and x!=None],stdout=subprocess.DEVNULL,stderr=subprocess.DEVNULL)
        if run_result.returncode==0:
            break
        else:
            tqdm.tqdm.write("jvm failed")
            lower=upper
            upper=round(upper*2)

    while abs(upper-lower)>2:
        memory_amount = (lower+upper)//2
        memory_flag = f"-Xmx{memory_amount}m"
        gc_flag = vars.gc_map[config["gc"]]

        tqdm.tqdm.write(f"Trying {memory_amount}m")
        run_line = f"{config['jdk']}/build/linux-x86_64-server-release/jdk/bin/java {gc_flag} {memory_flag} -XX:+DisableExplicitGC -Xlog:gc -jar {dacapo_location} {config['dacapo_benchmark']} -n3"
        run_result = subprocess.run([x for x in run_line.split(" ") if x!="" and x!=None],stdout=subprocess.DEVNULL,stderr=subprocess.DEVNULL)
        if run_result.returncode==0:
            tqdm.tqdm.write("jvm succeeded")
            upper=memory_amount
        else:
            tqdm.tqdm.write("jvm failed")
            lower=memory_amount

    with open(f"{directory}/min_val.txt","w") as f:
        f.write(f"{upper}")
    tqdm.tqdm.write(f"The min memory value is:{upper}")



def collect_results(config):
    directory=config["directory"]
    min_memory=None
    return_value = {"min_memory":0,"failed":False}
    if not os.path.isfile(f"{directory}/min_val.txt"):
        return_value["failed"]=True
        return return_value
    with open(f"{directory}/min_val.txt","r")as f :
        min_memory=int(f.read())
    return [return_value]


if __name__!="__main__":
    configs = vars.benchmark_configs
    info = {}
    info["run"]=run
    info["collect_results"]=collect_results
    configs["dacapo_find_min"]=info
