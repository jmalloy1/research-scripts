#!/usr/bin/env python3
import vars

import time
import threading
import subprocess
import tqdm
import os
import math
import sys


def individual_run(config,memory_flag,timeout):
        dacapo_location=config["dacapo_location"]
        gc_flag = vars.gc_map[config["gc"]]
        run_line = f"{config['jdk']}/build/linux-x86_64-server-release/jdk/bin/java {gc_flag} {memory_flag} -XX:+DisableExplicitGC -Xlog:gc -jar {dacapo_location} {config['dacapo_benchmark']} -n3"
        try:
            if timeout !=0:
                run_result = subprocess.run([x for x in run_line.split(" ") if x!="" and x!=None],stdout=subprocess.DEVNULL,stderr=subprocess.DEVNULL,timeout=timeout)
            else:
                run_result = subprocess.run([x for x in run_line.split(" ") if x!="" and x!=None],stdout=subprocess.DEVNULL,stderr=subprocess.DEVNULL)
        except subprocess.TimeoutExpired:
            tqdm.tqdm(f"Timeout Reached {memory_flag}");
            with open(f"{config['directory']}/timeouts.txt","a") as f:
                f.write(f"Timeout Reached {memory_flag}")
            return 1
        return run_result.returncode

def run(config):
    lower = 2
    upper = 2
    memory_amount = None
    timeout=None;
    directory = config["directory"]


    if not os.path.isdir(f"/tmp/tmp-0"):
        os.mkdir(f"/tmp/tmp-0")

    timeout = time.time()

    memory_flag = f""
    gc_flag = vars.gc_map[config["gc"]]
    individual_run(config,"",0)
    timeout = time.time()-timeout

    while True:
        tqdm.tqdm.write(f"Walking up {upper}m")
        memory_flag = f"-Xmx{upper}m"

        if individual_run(config,memory_flag,timeout*10)==0:
            break
        else:
            tqdm.tqdm.write("jvm failed")
            lower=upper
            upper=round(upper*2)

    while abs(upper-lower)>2:
        memory_amount = (lower+upper)//2
        memory_flag = f"-Xmx{memory_amount}m"
        gc_flag = vars.gc_map[config["gc"]]

        tqdm.tqdm.write(f"Trying {memory_amount}m")
        if individual_run(config,memory_flag,timeout*10)==0:
            tqdm.tqdm.write("jvm succeeded")
            upper=memory_amount
        else:
            tqdm.tqdm.write("jvm failed")
            lower=memory_amount

    with open(f"{directory}/min_val.txt","w") as f:
        f.write(f"{upper}")
    tqdm.tqdm.write(f"The min memory value is:{upper}")


def collect_results(config):
    directory=config["directory"]
    min_memory=None
    if not os.path.isfile(f"{directory}/min_val.txt"):
        return {"min_memory":None}
    with open(f"{directory}/min_val.txt","r")as f :
        min_memory=int(f.read())
    return [{"min_memory":min_memory}]

if __name__!="__main__":
    configs = vars.benchmark_configs
    info = {}
    info["run"]=run
    info["collect_results"]=collect_results
    configs["dacapo_find_min_timeout"]=info
