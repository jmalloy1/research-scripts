#!/usr/bin/env python3
import vars

import time
import threading
import subprocess
import tqdm
import os
import math
import re
import signal
import resource


done_condition = threading.Condition()
processes=None
continue_memory_measures = None
cpu_time_regex=None
iteration_regex=None
memory_statistics_regex=None

pause_time_regex=None
memory_used_regex=None
llc_load_regex = None
llc_miss_regex = None

benchmark_opts = {
  "avrora"       : { "DACAPO_ITERATIONS" : 10 },
  "batik"        : { "DACAPO_ITERATIONS" : 50 },
  "biojava"      : { "DACAPO_ITERATIONS" : 10 },
  "cassandra"    : { "JAVA_OPTS" : [ "-Djava.security.manager=allow" ],
                     "DACAPO_ITERATIONS" : 10 },
  "eclipse"      : { "DACAPO_ITERATIONS" : 10 },
  "fop"          : { "DACAPO_ITERATIONS" : 100},
  "graphchi"     : { "DACAPO_ITERATIONS" : 10 },
  "h2"           : { "DACAPO_ITERATIONS" : 10 },
  "h2o"          : { "JAVA_OPTS" : [ "-Dsys.ai.h2o.debug.allowJavaVersions=24" ],
                     "DACAPO_ITERATIONS" : 10 },
  "jme"          : { "DACAPO_ITERATIONS" : 10 },
  "jython"       : { "DACAPO_ITERATIONS" : 10 },
  "kafka"        : { "DACAPO_ITERATIONS" : 10 },
  "luindex"      : { "DACAPO_ITERATIONS" : 10 },
  "lusearch"     : { "DACAPO_ITERATIONS" : 10 },
  "pmd"          : { "DACAPO_ITERATIONS" : 30 },
  "spring"       : { "DACAPO_ITERATIONS" : 30 },
  "sunflow"      : { "DACAPO_ITERATIONS" : 30 },
  "tomcat"       : { "DACAPO_ITERATIONS" : 20 },
  #"tradebeans"   : { "DACAPO_ITERATIONS" : 10 },
  #"tradesoap"    : { "DACAPO_ITERATIONS" : 10 },
  "xalan"        : { "DACAPO_ITERATIONS" : 50 },
  "zxing"        : { "DACAPO_ITERATIONS" : 50 },
}

def setlimits():
    resource.setrlimit(resource.RLIMIT_OFILE,(300_000,300_000))

def spawn_process(config,number):
    global processes


    dacapo_location=config["dacapo_location"]
    dacapo_benchmark = config["dacapo_benchmark"]
    dacapo_benchmark_opts = benchmark_opts[dacapo_benchmark]
    #print(dacapo_benchmark_opts)
    dacapo_iterations = dacapo_benchmark_opts["DACAPO_ITERATIONS"]
    dacapo_java_opt = "" if "JAVA_OPTS" not in dacapo_benchmark_opts else " ".join(dacapo_benchmark_opts["JAVA_OPTS"])



    if not os.path.isdir(f"/tmp/tmp-{number}"):
        os.mkdir(f"/tmp/tmp-{number}")

    p=None

    memory_flag = "" if config["memory_config"]=="default" else f"-Xmx{config['memory_config']}m"
    gc_flag = vars.gc_map[config["gc"]]

    logging_flags = "" if "gc_logging" not in config else config["gc_logging"]

    soft_max = "" if "softmax" not in config else f"-XX:SoftMaxHeapSize={config['softmax']}"


    java_flags = f"{gc_flag} {memory_flag} {logging_flags} {soft_max} {dacapo_java_opt} -XX:+DisableExplicitGC"



    run_line = f"/usr/bin/time -v {config['jdk']}/build/linux-x86_64-server-release/jdk/bin/java {java_flags} -jar {dacapo_location} {dacapo_benchmark} -n{dacapo_iterations}"
    run_line = re.sub(' +',' ',run_line)
    tqdm.tqdm.write(f"{run_line}\n")
    #quit()

    with open(f"{config['directory']}/runner{number}.sh","w") as f:
        f.write("#!/usr/bin/env bash\n")
        #f.write("sleep 10\n")
        f.write(f"{run_line}\n")

    with open(f"{config['directory']}/threadstat.csv","w") as file:
        threadstat_run_line = f"{config['threadstat_location']}/threadstats"
        threadstat = subprocess.Popen([x for x in threadstat_run_line.split(" ") if x!="" and x!=None],stderr=None,stdout=file,preexec_fn=setlimits)

    new_env = os.environ.copy()
    new_env["LD_PRELOAD"]=f"{config['threadstat_location']}/wrapper.so"
    with open(f"{config['directory']}/jvm{number}.txt","w") as file:
        run_line2 = f"bash {config['directory']}/runner{number}.sh"
        p = subprocess.Popen([x for x in run_line2.split(" ") if x!="" and x!=None],stderr=file,stdout=file,cwd=f"/tmp/tmp-{number}/",env=new_env)
    processes.append(p)

    with open(f"/sys/fs/cgroup/jvm-cgroup/cgroup.procs","w") as file:
        file.write(f"{p.pid}\n")
        file.flush()





    p.wait()
    threadstat.send_signal(signal.SIGINT)
    threadstat.wait()
    with done_condition:
        done_condition.notify()



def percentile(list,percent):
    sorted_input = sorted(list)
    mid = len(sorted_input) * percent
    return sorted_input[math.floor(mid)] if math.ceil(mid)>=len(sorted_input) else (sorted_input[math.floor(mid)]+sorted_input[math.ceil(mid)])/2

def get_memory_info(list,prefix):
    list = sorted(list)
    tmp_avg = sum(list)/len(list)
    tmp_std = (sum([((x - tmp_avg) ** 2) for x in list]) / len(list))**.5
    return f"{prefix}_max:{max(list)}\n{prefix}_avg:{tmp_avg}\n{prefix}_std:{tmp_std}\n{prefix}_med:{percentile(list,.5)}\n{prefix}_90:{percentile(list,.9)}\n{prefix}_95:{percentile(list,.95)}\n{prefix}_99:{percentile(list,.99)}\n"


def check_timeout(process):
    try:
        process.wait(timeout=1)
        return True
    except subprocess.TimeoutExpired:
        return False


def kill_processes(processes):
    old_time = time.time()
    while len(processes)>0:
        for i in processes:
            os.system(f"pkill -15 -P {i.pid}")
        processes=[i for i in processes if check_timeout(i)==False]



def run(config):
    #config_time = time.time()
    memory_measures=[]
    global processes
    global continue_memory_measures
    processes=[]
    threads = []
    continue_memory_measures = True
    directory = config["directory"]



    #tqdm.tqdm.write(directory)


    for i in range(config["process_count"]):
        t = threading.Thread(target=spawn_process,args=(config,i))
        t.start()
        threads.append(t)




    with done_condition:
        done_condition.wait()


    kill_processes(processes)

    for t in threads:
        t.join()

#     tqdm.tqdm.write(f"Time to print memory usage:{time.time()-old_time}")
#     tqdm.tqdm.write(f"Total Time:{time.time()-config_time}")


gc_barrier_slow_paths_regex = re.compile(r"Barrier slow paths:(\d*)")
def get_gc_barrier_slow_paths(input_string):
    search_value = gc_barrier_slow_paths_regex.search(input_string)
    if search_value == None:
        return None;
    else:
        return int(search_value.group(1))

def collect_results(config):
    try:
        global cpu_time_regex
        count = 0
        allocation_stall_count=0
        total_cpu_time = 0
        z_cpu_time=0
        memory_print_info = []

        gc_barrier_slow_path_count = 0
        pause_time = 0.0
        memory_used_kb = 0;
        llc_loads = 0;
        llc_misses = 0;

        return_data = {}
        return_data["failed"]=False
        dirName=config["directory"]
        its = []
        for i in os.listdir(config["directory"]):
            if(i[:3]!="jvm"):
                continue
            with open(f"{dirName}/{i}") as f:
                tmp_file_in=f.read()
                its = iteration_regex.findall(tmp_file_in)
                pause_time_regex_results = pause_time_regex.findall(tmp_file_in)
                tmp_pause_time = sum([float(i) for i in pause_time_regex_results])
                allocation_stall_count+=tmp_file_in.count("Allocation Stall")
                cpu_time_result = [ sum(x) for x in list(map(list,zip(*[(float(i[1]),float(i[1]) if i[0] =="Z" or i[0] =="G" else 0.0) for i in cpu_time_regex.findall(tmp_file_in.split("JNI global refs:")[0])])))]
                if len(cpu_time_result)>=2:
                    if(cpu_time_result[1]==0.0):
                        print(att_list)
                    total_cpu_time += cpu_time_result[0]
                    z_cpu_time+=cpu_time_result[1]
                memory_print_info += [{"gc_time":float(i[0]),"gc_number":int(i[1]),"gc_capacity":int(i[2]),"gc_free":int(i[3]),"gc_live":int(i[4]),"gc_used":int(i[5])} for i in memory_statistics_regex.findall(tmp_file_in)]

                memory_used_kb += int(memory_used_regex.search(tmp_file_in).groups()[0])
                llc_loads += int(llc_load_regex.search(tmp_file_in).groups()[0].replace(",",""))
                llc_misses += int(llc_miss_regex.search(tmp_file_in).groups()[0].replace(",",""))
                gc_barrier_slow_paths = get_gc_barrier_slow_paths(tmp_file_in)
                gc_barrier_slow_path_count += 0 if gc_barrier_slow_paths == None else gc_barrier_slow_paths
                pause_time += tmp_pause_time


        if os.path.isfile(f"{dirName}/fail.txt"):
            return_data["failed"]=True
        return_data["total_cpu_time"]=total_cpu_time
        return_data["z_cpu_time"]=z_cpu_time
        return_data["allocation_stall_count"]=allocation_stall_count

        return_data["gc_barrier_slow_path"] = gc_barrier_slow_path_count
        return_data["gc_pause_time_ms"] = pause_time
        return_data["memory_used_kb"] = memory_used_kb
        return_data["llc_misses"] = llc_misses
        return_data["llc_loads"] = llc_loads



        if not os.path.isfile(f"{dirName}/memory-usage.txt"):
            return_data["failed"]=True
        else:
            with open(f"{dirName}/memory-usage.txt") as f:
                memory_file_tmp=f.read()
                for line in memory_file_tmp.split("\n"):
                    line_split = line.split(":")
                    if len(line_split)!=2:
                        continue
                    return_data[line_split[0]]=line_split[1]
        #print(memory_print_info)
        return_value = [{**return_data,**{"dacapo_iteration":i,"dacapo_sample_measurement_type":"dacapo_iteration"}} for i in its]
        return_value += [{**return_data,**i,**{"dacapo_sample_measurement_type":"benchmark_iteration"}} for i in memory_print_info]
        return return_value
    except(e):
        pass




if __name__!="__main__":


    configs = vars.benchmark_configs
    config={}
    config["run"]=run
    config["collect_results"]=collect_results
    configs["dacapo_samples2"]=config

    iteration_regex = re.compile(r"DaCapo.*in (\d*) msec")
    cpu_time_regex = re.compile(r"^\"([a-zA-Z])[a-zA-Z_# :0-9]*\".* os_prio=[0-9]* cpu=([0-9.]*)ms",re.MULTILINE)
    memory_statistics_regex = re.compile(r"\[(\d+\.\d+)s\]\[info\]\[gc,Jacob\] GC\((\d+)\) Mark Stat: Capacity: (\d+) Free: (\d+) Live: (\d+) Used: (\d+)")
    pause_time_regex = re.compile(r"Pause[A-Za-z ()]*([0-9]+\.[0-9]+)ms")
    memory_used_regex = re.compile(r"Maximum resident set size \(kbytes\): ([0-9]+)")
    llc_load_regex = re.compile(r"([0-9,]+)\s*LLC-loads")
    llc_miss_regex = re.compile(r"([0-9,]+)\s*LLC-load-misses")
