#!/usr/bin/env python3
import vars

import time
import threading
import subprocess
import tqdm
import os
import math
import re


failed_condition = threading.Condition()
processes=None
continue_memory_measures = None
cpu_time_regex=None

def spawn_process(config,number):
    dacapo_location=config["dacapo_location"]
    global processes


    if not os.path.isdir(f"/tmp/tmp-{number}"):
        os.mkdir(f"/tmp/tmp-{number}")

    p=None

    memory_flag = "" if config["memory_config"]=="default" else f"-Xmx{config['memory_config']}m"
    gc_flag = vars.gc_map[config["gc"]]

    gc_cpus_flag = "" if "gc_cpus" not in config else f"-XX:GCCPUS={config['gc_cpus']}"

    run_line = f"/usr/bin/time -v {config['jdk']}/build/linux-x86_64-server-release/jdk/bin/java {gc_flag} {gc_cpus_flag} {memory_flag} -XX:+DisableExplicitGC -Xlog:gc -jar {dacapo_location} {config['dacapo_benchmark']} -n100000"
    tqdm.tqdm.write(f"{run_line}\n")
    with open(f"{config['directory']}/jvm{number}.txt","w") as file:
        p = subprocess.Popen([x for x in run_line.split(" ") if x!="" and x!=None],stderr=file,stdout=file,cwd=f"/tmp/tmp-{number}/")
    processes.append(p)
    with open(f"/sys/fs/cgroup/jvm-cgroup/cgroup.procs","w") as file:
            file.write(f"{p.pid}\n")
            file.flush()

    p.wait()
    with failed_condition:
        failed_condition.notify()



def percentile(list,percent):
    sorted_input = sorted(list)
    mid = len(sorted_input) * percent
    return sorted_input[math.floor(mid)] if math.ceil(mid)>=len(sorted_input) else (sorted_input[math.floor(mid)]+sorted_input[math.ceil(mid)])/2

def get_memory_info(list,prefix):
    list = sorted(list)
    tmp_avg = sum(list)/len(list)
    tmp_std = (sum([((x - tmp_avg) ** 2) for x in list]) / len(list))**.5
    return f"{prefix}_max:{max(list)}\n{prefix}_avg:{tmp_avg}\n{prefix}_std:{tmp_std}\n{prefix}_med:{percentile(list,.5)}\n{prefix}_90:{percentile(list,.9)}\n{prefix}_95:{percentile(list,.95)}\n{prefix}_99:{percentile(list,.99)}\n"


def memory_usage(directory):
    memory_measures=[]
    while continue_memory_measures:
        with open(f"/sys/fs/cgroup/jvm-cgroup/memory.stat","r") as file:
            tmp_memory_file_content=file.read()
            memory_measures.append(tuple([int(line.split(" ")[1]) for line in tmp_memory_file_content.split("\n") if len(line)>0]))
            # was doing something with memory_names, don't remmember what that was
            memory_names=[line.split(" ")[0] for line in tmp_memory_file_content.split("\n") if len(line)>=0]
        time.sleep(0.5)

    with open(f"{directory}/memory-usage.txt","w+") as out_file:
        out_string=""
        for i in zip(memory_names,list(map(list, zip(*memory_measures)))):
            out_string += get_memory_info(i[1],i[0])
        out_file.write(out_string)
        out_file.flush()

def check_timeout(process):
    try:
        process.wait(timeout=1)
        return True
    except subprocess.TimeoutExpired:
        return False


def kill_processes(processes):
    old_time = time.time()
    while len(processes)>0:
        for i in processes:
            os.system(f"pkill -15 -P {i.pid}")
        processes=[i for i in processes if check_timeout(i)==False]



def run(config):
    #config_time = time.time()
    memory_measures=[]
    global processes
    global continue_memory_measures
    processes=[]
    threads = []
    continue_memory_measures = True
    directory = config["directory"]



    #tqdm.tqdm.write(directory)


    for i in range(config["process_count"]):
        t = threading.Thread(target=spawn_process,args=(config,i))
        t.start()
        threads.append(t)


    memory_thread = threading.Thread(target=memory_usage,args=(directory,))
    memory_thread.start()


    with failed_condition:
        if failed_condition.wait(timeout=config["time_before_kill_secs"])==True:
            tqdm.tqdm.write("Failed this benchmark")
            with open(f"{directory}/fail.txt","w") as f:
                f.write("A JVM instance crashed")

    continue_memory_measures=False

    kill_processes(processes)

    for t in threads:
        t.join()
    memory_thread.join()

#     tqdm.tqdm.write(f"Time to print memory usage:{time.time()-old_time}")
#     tqdm.tqdm.write(f"Total Time:{time.time()-config_time}")


def collect_results(config):
    global cpu_time_regex
    count = 0
    allocation_stall_count=0
    total_cpu_time = 0
    z_cpu_time=0

    return_data = {}
    return_data["failed"]=False
    dirName=config["directory"]
    for i in os.listdir(config["directory"]):
        if(i[:3]!="jvm"):
            continue
        with open(f"{dirName}/{i}") as f:
            tmp_file_in=f.read()
            count += tmp_file_in.count("completed")
            allocation_stall_count+=tmp_file_in.count("Allocation Stall")
            cpu_time_result = [ sum(x) for x in list(map(list,zip(*[(float(i[1]),float(i[1]) if i[0] =="Z" or i[0] =="G" else 0.0) for i in cpu_time_regex.findall(tmp_file_in.split("JNI global refs:")[0])])))]
            if len(cpu_time_result)>=2:
                if(cpu_time_result[1]==0.0):
                    print(att_list)
                total_cpu_time += cpu_time_result[0]
                z_cpu_time+=cpu_time_result[1]
    if os.path.isfile(f"{dirName}/fail.txt"):
        return_data["failed"]=True
    return_data["total_cpu_time"]=total_cpu_time
    return_data["z_cpu_time"]=z_cpu_time
    return_data["allocation_stall_count"]=allocation_stall_count
    return_data["iteration_count"]=count



    if not os.path.isfile(f"{dirName}/memory-usage.txt"):
        return_data["failed"]=True
        return return_data



    with open(f"{dirName}/memory-usage.txt") as f:
        memory_file_tmp=f.read()
        for line in memory_file_tmp.split("\n"):
            line_split = line.split(":")
            if len(line_split)!=2:
                continue
            return_data[line_split[0]]=line_split[1]
    return [return_data]
#    return ("/".join(att_list),(count,avg_bytes,allocation_stall_count,total_cpu_time,z_cpu_time,max_bytes,"fail.txt" in fileList))





if __name__!="__main__":


    configs = vars.benchmark_configs
    config={}
    config["run"]=run
    config["collect_results"]=collect_results
    configs["dacapo_memory"]=config

    cpu_time_regex = re.compile(r"^\"([a-zA-Z])[a-zA-Z_# :0-9]*\".* os_prio=[0-9]* cpu=([0-9.]*)ms",re.MULTILINE)
