#!/usr/bin/env python3
import vars

import time
import threading
import subprocess
import tqdm
import os
import math
import re


done_condition = threading.Condition()
processes=None
continue_memory_measures = None
cpu_time_regex=None
iteration_regex=None
memory_statistics_regex=None

def spawn_process(config,number):
    dacapo_location=config["dacapo_location"]
    global processes


    if not os.path.isdir(f"/tmp/tmp-{number}"):
        os.mkdir(f"/tmp/tmp-{number}")

    p=None

    memory_flag = "" if config["memory_config"]=="default" else f"-Xmx{config['memory_config']}m"
    gc_flag = vars.gc_map[config["gc"]]

    gc_cpus_flag = "" if "gc_cpus" not in config else f"-XX:GCCPUS={config['gc_cpus']}"

    #logging_flags = "-Xlog:gc+Jacob" if "jacob_memory_printing" in config and config["jacob_memory_printing"]=="True" else "-Xlog:gc"

    #logging_flags = "" if "jacob_gc_pause_time" not in config else f"-XX:GCPauseTime={config['jacob_gc_pause_time']}"
    logging_flags = "" if "gc_logging" not in config else config["gc_logging"]

    soft_max = "" if "softmax" not in config else f"-XX:SoftMaxHeapSize={config['softmax']}"

    run_line = f"/usr/bin/time -v {config['jdk']}/build/linux-x86_64-server-release/jdk/bin/java {gc_flag} {soft_max} {gc_cpus_flag} {memory_flag} -XX:+DisableExplicitGC {logging_flags} -jar {dacapo_location} {config['dacapo_benchmark']} -n110"
    tqdm.tqdm.write(f"{run_line}\n")
    #quit()
    with open(f"{config['directory']}/jvm{number}.txt","w") as file:
        p = subprocess.Popen([x for x in run_line.split(" ") if x!="" and x!=None],stderr=file,stdout=file,cwd=f"/tmp/tmp-{number}/")
    processes.append(p)
    with open(f"/sys/fs/cgroup/jvm-cgroup/cgroup.procs","w") as file:
        file.write(f"{p.pid}\n")
        file.flush()

    p.wait()
    with done_condition:
        done_condition.notify()



def percentile(list,percent):
    sorted_input = sorted(list)
    mid = len(sorted_input) * percent
    return sorted_input[math.floor(mid)] if math.ceil(mid)>=len(sorted_input) else (sorted_input[math.floor(mid)]+sorted_input[math.ceil(mid)])/2

def get_memory_info(list,prefix):
    list = sorted(list)
    tmp_avg = sum(list)/len(list)
    tmp_std = (sum([((x - tmp_avg) ** 2) for x in list]) / len(list))**.5
    return f"{prefix}_max:{max(list)}\n{prefix}_avg:{tmp_avg}\n{prefix}_std:{tmp_std}\n{prefix}_med:{percentile(list,.5)}\n{prefix}_90:{percentile(list,.9)}\n{prefix}_95:{percentile(list,.95)}\n{prefix}_99:{percentile(list,.99)}\n"


def memory_usage(directory):
    memory_measures=[]
    while continue_memory_measures:
        with open(f"/sys/fs/cgroup/jvm-cgroup/memory.stat","r") as file:
            tmp_memory_file_content=file.read()
            memory_measures.append(tuple([int(line.split(" ")[1]) for line in tmp_memory_file_content.split("\n") if len(line)>0]))
            # was doing something with memory_names, don't remmember what that was
            memory_names=[line.split(" ")[0] for line in tmp_memory_file_content.split("\n") if len(line)>=0]
        time.sleep(0.5)

    with open(f"{directory}/memory-usage.txt","w+") as out_file:
        out_string=""
        for i in zip(memory_names,list(map(list, zip(*memory_measures)))):
            out_string += get_memory_info(i[1],i[0])
        out_file.write(out_string)
        out_file.flush()

def check_timeout(process):
    try:
        process.wait(timeout=1)
        return True
    except subprocess.TimeoutExpired:
        return False


def kill_processes(processes):
    old_time = time.time()
    while len(processes)>0:
        for i in processes:
            os.system(f"pkill -15 -P {i.pid}")
        processes=[i for i in processes if check_timeout(i)==False]



def run(config):
    #config_time = time.time()
    memory_measures=[]
    global processes
    global continue_memory_measures
    processes=[]
    threads = []
    continue_memory_measures = True
    directory = config["directory"]



    #tqdm.tqdm.write(directory)


    for i in range(config["process_count"]):
        t = threading.Thread(target=spawn_process,args=(config,i))
        t.start()
        threads.append(t)


    memory_thread = threading.Thread(target=memory_usage,args=(directory,))
    memory_thread.start()


    with done_condition:
        done_condition.wait()

    continue_memory_measures=False

    kill_processes(processes)

    for t in threads:
        t.join()
    memory_thread.join()

#     tqdm.tqdm.write(f"Time to print memory usage:{time.time()-old_time}")
#     tqdm.tqdm.write(f"Total Time:{time.time()-config_time}")


gc_barrier_slow_paths_regex = re.compile(r"Barrier slow paths:(\d*)")
def get_gc_barrier_slow_paths(input_string):
    search_value = gc_barrier_slow_paths_regex.search(input_string)
    if search_value == None:
        return None;
    else:
        return int(search_value.group(1))

def collect_results(config):
    try:
        global cpu_time_regex
        count = 0
        allocation_stall_count=0
        total_cpu_time = 0
        z_cpu_time=0
        memory_print_info = []

        gc_barrier_slow_path_count = 0

        return_data = {}
        return_data["failed"]=False
        dirName=config["directory"]
        its = []
        for i in os.listdir(config["directory"]):
            if(i[:3]!="jvm"):
                continue
            with open(f"{dirName}/{i}") as f:
                tmp_file_in=f.read()
                its = iteration_regex.findall(tmp_file_in)
                allocation_stall_count+=tmp_file_in.count("Allocation Stall")
                cpu_time_result = [ sum(x) for x in list(map(list,zip(*[(float(i[1]),float(i[1]) if i[0] =="Z" or i[0] =="G" else 0.0) for i in cpu_time_regex.findall(tmp_file_in.split("JNI global refs:")[0])])))]
                if len(cpu_time_result)>=2:
                    if(cpu_time_result[1]==0.0):
                        print(att_list)
                    total_cpu_time += cpu_time_result[0]
                    z_cpu_time+=cpu_time_result[1]
                memory_print_info += [{"gc_time":float(i[0]),"gc_number":int(i[1]),"gc_capacity":int(i[2]),"gc_free":int(i[3]),"gc_live":int(i[4]),"gc_used":int(i[5])} for i in memory_statistics_regex.findall(tmp_file_in)]

                gc_barrier_slow_paths = get_gc_barrier_slow_paths(tmp_file_in)
                gc_barrier_slow_path_count += 0 if gc_barrier_slow_paths == None else gc_barrier_slow_paths


        if os.path.isfile(f"{dirName}/fail.txt"):
            return_data["failed"]=True
        return_data["total_cpu_time"]=total_cpu_time
        return_data["z_cpu_time"]=z_cpu_time
        return_data["allocation_stall_count"]=allocation_stall_count

        return_data["gc_barrier_slow_path"] = gc_barrier_slow_path_count



        if not os.path.isfile(f"{dirName}/memory-usage.txt"):
            return_data["failed"]=True
        else:
            with open(f"{dirName}/memory-usage.txt") as f:
                memory_file_tmp=f.read()
                for line in memory_file_tmp.split("\n"):
                    line_split = line.split(":")
                    if len(line_split)!=2:
                        continue
                    return_data[line_split[0]]=line_split[1]
        #print(memory_print_info)
        return_value = [{**return_data,**{"dacapo_iteration":i,"dacapo_sample_measurement_type":"sample_time"}} for i in its]
        return_value += [{**return_data,**i,**{"dacapo_sample_measurement_type":"gc_memory_info"}} for i in memory_print_info]
        return return_value
    except(e):
        pass




if __name__!="__main__":


    configs = vars.benchmark_configs
    config={}
    config["run"]=run
    config["collect_results"]=collect_results
    configs["dacapo_samples"]=config

    iteration_regex = re.compile(r"DaCapo.*in (\d*) msec")
    cpu_time_regex = re.compile(r"^\"([a-zA-Z])[a-zA-Z_# :0-9]*\".* os_prio=[0-9]* cpu=([0-9.]*)ms",re.MULTILINE)
    memory_statistics_regex = re.compile(r"\[(\d+\.\d+)s\]\[info\]\[gc,Jacob\] GC\((\d+)\) Mark Stat: Capacity: (\d+) Free: (\d+) Live: (\d+) Used: (\d+)")
