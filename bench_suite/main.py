#!/usr/bin/env python3



import dacapo_memory
import dacapo_samples
import dacapo_samples2
import dacapo_find_min
import dacapo_find_min_timeout
import renaissance_find_min_timeout


import vars



import subprocess
import os
import threading
import time
import math
from pathlib import Path
import itertools
import tqdm
import termios
import sys
import atexit
import json
import datetime
import platform
import multiprocessing
import pandas as pd
import concurrent.futures

PAUSE_MUTEX_LOCK=threading.Lock()

CGROUP="jvm-cgroup"




def enable_echo(enable):
    fd = sys.stdin.fileno()
    new = termios.tcgetattr(fd)
    if enable:
        new[3] |= termios.ECHO
    else:
        new[3] &= ~termios.ECHO

    termios.tcsetattr(fd, termios.TCSANOW, new)


def input_thread_function():
    global PAUSE_MUTEX_LOCK
    while 1:
        while input()!="pause":
            pass
        PAUSE_MUTEX_LOCK.acquire()
        try:
            tqdm.tqdm.write("pausing - type \"continue\"  to continue")
        except:
            tqdm.tqdm.write("pausing - type \"continue\"  to continue")
        while input() !="continue":pass
        PAUSE_MUTEX_LOCK.release()


def write_bench_suite_status(bench_suite_status,location):
    with open(f"{location}/status.json","w") as f :
        f.write(json.dumps(bench_suite_status))


def generate_indexed_data(benchmarks):
    return_value = {}
    for key,value in benchmarks.items():
        for key2,value2 in value.items():
            if key2 not in return_value:
                return_value[key2]={}
            if value2 not in return_value[key2]:
                return_value[key2][value2]=set()
            return_value[key2][value2].add(int(key))
    return return_value


def run_collect_for_config(config):
    current_benchmark = vars.benchmark_configs[config["benchmark"]]
    tmp = current_benchmark["collect_results"](config)
    return (config,tmp)


def main():


    #This sets up the stdin not echoing out
    atexit.register(enable_echo, True)
    enable_echo(False)


    #Make sure that transparent_hugepages is off
    with open("/sys/kernel/mm/transparent_hugepage/enabled","w") as file:
        file.write("never")

    #I think I will leave this cgroup code for now I think we will just assume that the cgroup will be used
    #This may be a bad assumption
    if not os.path.isdir(f"/sys/fs/cgroup/{CGROUP}"):
        os.mkdir(f"/sys/fs/cgroup/{CGROUP}")
    with open(f"/sys/fs/cgroup/{CGROUP}/memory.max","w") as file:
        file.write(f"max");

    memory_names=None
    input_thread = threading.Thread(target=input_thread_function)
    input_thread.daemon=True
    input_thread.start()



    config=None
    if len(sys.argv)<2:
        print("Please provide a config file as an argument",file=sys.stderr)
        sys.exit(1)
    with open(sys.argv[1],"r") as f:
        config_text = f.read()
        config = json.loads(config_text)

    if not os.path.isdir(config["location"]):
        os.mkdir(config["location"])


    bench_suite_status={"bench_index":0,"benchmark_runs":{}}
    if os.path.isfile(f"{config['location']}/status.json"):
        with open(f"{config['location']}/status.json","r") as f:
            config_text = f.read()
            bench_suite_status = json.loads(config_text)

    write_bench_suite_status(bench_suite_status,config["location"])

    configs_to_run=[]
    if "run" in config:
        for runnable in config["run"]:
            comb = itertools.product(*runnable.values())
            configs_to_run.extend([{i[0]:i[1] for i in zip(runnable.keys(),x)} for x in comb])


    #log the test start time
    start_date_time = datetime.datetime.now().isoformat("#")

    for current_config in tqdm.tqdm(configs_to_run):
        current_benchmark=vars.benchmark_configs[current_config["benchmark"]]


        # setup the automatic parameters
        current_config["datetime"]=datetime.datetime.now().isoformat("#")
        current_config["start_date_time"]=start_date_time
        git_rev_parse = subprocess.run("git rev-parse HEAD".split(" "),capture_output=True,cwd=current_config["jdk"])
        current_config["jdk_hash"] = "unknown" if git_rev_parse.returncode!=0 else git_rev_parse.stdout.decode("ascii").strip()
        git_rev_parse_abbrev = subprocess.run("git rev-parse --abbrev-ref HEAD".split(" "),capture_output=True,cwd=current_config["jdk"])
        current_config["jdk_branch_name"] = "unknown" if git_rev_parse_abbrev.returncode!=0 else git_rev_parse_abbrev.stdout.decode("ascii").strip()
        current_config["kernel"] = platform.version()
        current_config["cores"] = multiprocessing.cpu_count()
        bench_suite_hash = subprocess.run("git rev-parse HEAD".split(" "),capture_output=True)
        current_config["bench_suite_hash"] = "unknown" if bench_suite_hash.returncode!=0 else bench_suite_hash.stdout.decode("ascii").strip()
        current_config["directory"]=f"{config['location']}/runs/{bench_suite_status['bench_index']:016X}"


        bench_suite_status["benchmark_runs"][bench_suite_status["bench_index"]] = current_config
        bench_suite_status["bench_index"]+=1
        write_bench_suite_status(bench_suite_status,config["location"])

        if not os.path.isdir(current_config["directory"]):
            os.mkdir(current_config["directory"])



        tqdm.tqdm.write(str(current_config))

        #I think always dropping the caches is probably good
        with open("/proc/sys/vm/drop_caches","w") as drop_cache:
            drop_cache.write("3")

        current_benchmark["run"](current_config)


        if not PAUSE_MUTEX_LOCK.acquire(blocking=False):
            tqdm.tqdm.write("The main thread is now paused")
            PAUSE_MUTEX_LOCK.acquire()
        PAUSE_MUTEX_LOCK.release()




    indexed_benchmarks=generate_indexed_data(bench_suite_status["benchmark_runs"])
    for file_name,options in  config["collect"].items():
        output_data=[]
        results_to_get=set(range(bench_suite_status["bench_index"]))
        for key,val in options.items():
            tmp_set=set()
            for v in val:
                tmp_set|=indexed_benchmarks[key][v]
            results_to_get &= tmp_set
        configs_to_get = [bench_suite_status["benchmark_runs"][str(index)] for index in results_to_get]
        with concurrent.futures.ThreadPoolExecutor() as p:
            for t in tqdm.tqdm(p.map(run_collect_for_config,configs_to_get),total=len(configs_to_get)):
                if t is None:
                    continue
                current_config,parsed_data = t
                if parsed_data is None:
                    continue
                for i in parsed_data:
                    #print(f"current config: {type(current_config)}")
                    #print(f"i: {type(i)}")
                    #print(f"output_data: {type(output_data)}")
                    if type(i) is str:
                        print(f"PARSED DATA:\n\n{parsed_data}\n\n")
                    output_data.append({**current_config,**i})
        df=pd.DataFrame.from_dict(output_data).to_csv(f"{config['location']}/{file_name}",index=False,compression="gzip")



    #input_thread.exit()
    sys.exit()

if __name__=="__main__":
    os.setpgrp()
    main()
