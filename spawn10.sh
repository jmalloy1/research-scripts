#!/usr/bin/bash
JAVA="/home/jmalloy1/git/jdk-reduce/build/linux-x86_64-server-release/jdk/bin/java"
JAVA="/home/jmalloy1/git/jdk/build/linux-x86_64-server-release/jdk/bin/java"
JAVA_FLAGS="-XX:+UseZGC"
BENCHMARK="/home/jmalloy1/benchmarks/java/dacapo/dacapo-evaluation-git+309e1fa.jar h2 -n20"
CGROUP="jvm-cgroup"

echo "32212254720" > /sys/fs/cgroup/${CGROUP}/memory.max

for i in {1..10}; do
    mkdir tmp${i}
    cd tmp${i}
    echo "starting ${i}"
    $JAVA $JAVA_FLAGS -jar $BENCHMARK > ../jvm${i}.txt 2>&1 &
    echo $! >> /sys/fs/cgroup/${CGROUP}/cgroup.procs
    cd ..
    sleep 10
done


wait

rm -rf tmp*
