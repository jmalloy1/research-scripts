#!/usr/bin/python3
import subprocess
import os
import threading
import time
import re
from datetime import datetime

JAVA="/home/jmalloy1/git/jdk-reduce/build/linux-x86_64-server-release/jdk/bin/java"
#JAVA="/home/jmalloy1/git/jdk/build/linux-x86_64-server-release/jdk/bin/java"
JAVA_FLAGS="-XX:+UseZGC"
BENCHMARK="/home/jmalloy1/benchmarks/java/dacapo/dacapo-evaluation-git+309e1fa.jar h2 -n20"
CGROUP="jvm-cgroup"


num_maintained=0
not_killed=1


def thread(number):
    global JAVA
    global JAVA_FLAGS
    global BENCHMARK
    global CGROUP
    global num_maintained
    global not_killed

    count=0
    if not os.path.isdir(f"./tmp-{number}"):
        os.mkdir(f"./tmp-{number}")
    while count==0 or num_maintained<number:
        with open(f"jvm{number}-{count}.txt","w") as file:
            p = subprocess.Popen(f"{JAVA} {JAVA_FLAGS} -jar {BENCHMARK}".split(" "),stderr=file,stdout=file,cwd=f"./tmp-{number}/")
            with open(f"/sys/fs/cgroup/{CGROUP}/cgroup.procs", "w") as proc_file:
                proc_file.write(f"{p.pid}")
            p.wait()
            not_killed=0
        count +=1

def main():
    global JAVA
    global JAVA_FLAGS
    global BENCHMARK
    global CGROUP
    global num_maintained
    global not_killed


    if not os.path.isdir(f"/sys/fs/cgroup/{CGROUP}"):
        os.mkdir(f"/sys/fs/cgroup/{CGROUP}")
    with open(f"/sys/fs/cgroup/{CGROUP}/memory.max","w") as file:
        file.write(f"{64<<30}\n");

    sleep_till = datetime.timestamp(datetime.now())+(10*60)
    for i in range(16):
        print(f"spawning:{i}")
        x=threading.Thread(target=thread, args=(i,))
        x.start()
        should_continue = 1;
        while not_killed and should_continue:
            if not os.path.isfile(f"jvm{i}-0.txt"):
                time.sleep(.5)
                continue
            with open(f"jvm{i}-0.txt","r") as file:
                filetext=file.read()
                if re.search("completed",filetext):
                    should_continue=0
                    num_maintained+=1
            time.sleep(.5)
        if not_killed:
            continue
        print(f"steady-stat:{i}")
        break
    time.sleep(sleep_till-datetime.timestamp(datetime.now()))
    os.killpg(os.getpid(),9)


if __name__=="__main__":
    os.setpgrp()
    main()
