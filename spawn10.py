#!/usr/bin/python3
import subprocess
import os
import threading
import time

JAVA="/home/jmalloy1/git/jdk-reduce/build/linux-x86_64-server-release/jdk/bin/java"
JAVA="/home/jmalloy1/git/jdk/build/linux-x86_64-server-release/jdk/bin/java"
JAVA_FLAGS="-XX:+UseZGC"
BENCHMARK="/home/jmalloy1/benchmarks/java/dacapo/dacapo-evaluation-git+309e1fa.jar h2 -n20"
CGROUP="jvm-cgroup"

def thread(number):
    global JAVA
    global JAVA_FLAGS
    global BENCHMARK
    global CGROUP
    count=0
    if not os.path.isdir(f"./tmp-{number}"):
        os.mkdir(f"./tmp-{number}")
    while 1:
        with open(f"jvm{number}-{count}.txt","w") as file:
            p = subprocess.Popen(f"{JAVA} {JAVA_FLAGS} -jar {BENCHMARK}".split(" "),stderr=file,stdout=file,cwd=f"./tmp-{number}/")
            with open(f"/sys/fs/cgroup/{CGROUP}/cgroup.procs", "w") as proc_file:
                proc_file.write(f"{p.pid}")
            p.wait()
        count +=1

def main():
    global JAVA
    global JAVA_FLAGS
    global BENCHMARK
    global CGROUP


    if not os.path.isdir(f"/sys/fs/cgroup/{CGROUP}"):
        os.mkdir(f"/sys/fs/cgroup/{CGROUP}")
    with open(f"/sys/fs/cgroup/{CGROUP}/memory.max","w") as file:
        file.write(f"{30<<30}\n");
    for i in range(10):
        x=threading.Thread(target=thread, args=(i,))
        x.start()
    time.sleep(60*2.5)
    os.killpg(os.getpid(),9)

if __name__=="__main__":
    os.setpgrp()
    main()
