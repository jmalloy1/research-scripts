#!/bin/bash

FILE_NAME="/tmp/tmp-gdb-command-$$.txt"
echo "set breakpoint pending on" > ${FILE_NAME}
printf "${GDB_COMMAND}\n" >> ${FILE_NAME}
echo "run -XX:+UseZGC -jar /home/jmalloy1/testjava/Mem/test.jar" >> ${FILE_NAME}
#echo "run -XX:+UseZGC -jar /home/jmalloy1/benchmarks/java/dacapo/dacapo-evaluation-git+309e1fa.jar h2 -n1" >> ${FILE_NAME}

gdb /home/jmalloy1/git/jdk/build/linux-x86_64-server-fastdebug/jdk/bin/java -x ${FILE_NAME}

rm ${FILE_NAME}
