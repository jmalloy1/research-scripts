#!/usr/bin/python3
import subprocess
import os
import threading
import time
import signal
from pathlib import Path

#JAVA="/home/jmalloy1/git/jdk-reduce/build/linux-x86_64-server-release/jdk/bin/java"
JAVA="/home/jmalloy1/git/jdk/build/linux-x86_64-server-release/jdk/bin/java"
JAVA_FLAGS="-XX:+UseZGC"
BENCHMARK="/home/jmalloy1/benchmarks/java/dacapo/dacapo-evaluation-git+309e1fa.jar"
CGROUP="jvm-cgroup"

pids=set()
threads=[]
should_continue=True

def signal_handler(sig,frame):
    global threads
    global pids
    global should_continue

    should_continue=False

    for i in pids:
        os.pgkill(i,9)
    os.exit(0)

def thread(number,memory,benchmark):
    global JAVA
    global JAVA_FLAGS
    global BENCHMARK
    global CGROUP
    global pids
    global should_continue
    count=0
    p =0
    if not os.path.isdir(f"./tmp-{number}"):
        os.mkdir(f"./tmp-{number}")
    while should_continue:
        with open(f"tmp/jvm{number}-{count}.txt","w") as file:
            p = subprocess.Popen(f"{JAVA} {JAVA_FLAGS} {memory} -jar {BENCHMARK} {benchmark} -n1000".split(" "),stderr=file,stdout=file,cwd=f"./tmp-{number}/")
            pids.add(p.pid)
            with open(f"/sys/fs/cgroup/{CGROUP}/cgroup.procs", "w") as proc_file:
                proc_file.write(f"{p.pid}")
            p.wait()
        count +=1

def main():
    global JAVA
    global JAVA_FLAGS
    global BENCHMARK
    global CGROUP
    global pids
    global threads
    global should_continue
    #benchmarks=["h2","jme","jython","luindex","lusearch","pmd","sunflow","xalan","zxing"]
    benchmarks=["jme","jython","luindex","lusearch","pmd","sunflow","xalan","zxing"]
    execution_list=[("64","-Xmx64m","-Xms64m -Xmx64m"),("128","-Xmx128m","-Xms128m -Xmx128m"),("256","-Xmx256m","-Xms256m -Xmx256m"),("512","-Xmx512m","-Xms512m -Xmx512m"),("1024","-Xmx1024m","-Xms1024m -Xmx1024m"),("2048","-Xmx2048m","-Xms2048m -Xmx2048m"),("4096","-Xmx4096m","-Xms4096m -Xmx4096m"),("8192","-Xmx8192m","-Xms8192m -Xmx8192m")]
    max_memory = 0;
    threads = []

    signal.signal(signal.SIGINT,signal_handler)
    for set_memory in range(1,3):
        for bench in benchmarks:
            out_string="mb_limit,bytes\n"
            for k in range(10):
                for iterator in execution_list:
                    should_continue=1
                    max_memory=0
                    threads=[]
                    pids=set()
                    if not os.path.isdir(f"/sys/fs/cgroup/{CGROUP}"):
                        os.mkdir(f"/sys/fs/cgroup/{CGROUP}")
                    with open(f"/sys/fs/cgroup/{CGROUP}/memory.max","w") as file:
                        file.write(f"max");
                    for i in range(16):
                        x=threading.Thread(target=thread, args=(i,iterator[set_memory],bench))
                        x.start()
                        threads.append(x)
                    max_memory = 0
                    for i in range(300):
                        with open(f"/sys/fs/cgroup/{CGROUP}/memory.current","r") as file:
                            tmp = int(file.read())
                            max_memory = tmp if tmp>max_memory else max_memory
                        time.sleep(1)
                    should_continue=False
                    for i in pids:
                        try:
                            os.kill(i,9)
                        except OSError as err:
                            print(f"process {i} does not exist")
                    for i in threads:
                        i.join()
                    print(f"{iterator[0]} mb uses {max_memory} maximum bytes")
                    out_string+=f"{iterator[0]},{max_memory}\n"
                    Path(f"/home/jmalloy1/results/memory_usage/{bench}").mkdir(exist_ok=True,parents=True)
                    set_memory_string=""
                    if(set_memory==2):
                        set_memory_string="set-"
                    with open(f"/home/jmalloy1/results/memory_usage/{set_memory_string}{bench}-usage.csv","w+") as out_file:
                        out_file.write(out_string)
                        out_file.flush()

if __name__=="__main__":
    os.setpgrp()
    main()
