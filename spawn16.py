#!/usr/bin/env python3
import subprocess
import os
import threading
import time
import signal
import math
from pathlib import Path
import itertools
import tqdm
import threading
import termios
import sys
import atexit

#JAVA="/home/jmalloy1/git/jdk-reduce/build/linux-x86_64-server-release/jdk/bin/java"
JAVA="/home/jmalloy1/git/jdk/build/linux-x86_64-server-release/jdk/bin/java"
BENCHMARK="/home/jmalloy1/benchmarks/java/dacapo/dacapo-evaluation-git+309e1fa.jar"
CGROUP="jvm-cgroup"
#folder_name="fix-cpu-time"
folder_name="jdk-20"
PAUSE_MUTEX_LOCK=threading.Lock()

pids=set()
threads=[]


def enable_echo(enable):
    fd = sys.stdin.fileno()
    new = termios.tcgetattr(fd)
    if enable:
        new[3] |= termios.ECHO
    else:
        new[3] &= ~termios.ECHO

    termios.tcsetattr(fd, termios.TCSANOW, new)

def check_timeout(process):
    try:
        process.wait(timeout=1)
        return True
    except subprocess.TimeoutExpired:
        return False

def kill_processes(processes):
    old_time = time.time()
    while len(processes)>0:
        for i in processes:
            os.system(f"pkill -15 -P {i.pid}")
        processes=[i for i in processes if check_timeout(i)==False]

    tqdm.tqdm.write(f"Time to kill_process:{time.time()-old_time}")

def out_dir(benchmark,memory,gc,set_memory,iteration,pre_iteration_gc,num_processes):
    return f"/home/jmalloy1/results/{folder_name}/{num_processes}/{gc[0]}/{pre_iteration_gc[0]}/{set_memory[0]}/{benchmark}/{memory[0]}/{iteration}"


def spawn_process(number,benchmark,memory,gc,set_memory,pre_iteration_gc,directory):
    global JAVA
    global BENCHMARK
    global CGROUP


    if not os.path.isdir(f"./tmp-{number}"):
        os.mkdir(f"./tmp-{number}")

    p=None

    if not os.path.isdir(directory):
        os.makedirs(directory)
    with open(f"{directory}/jvm{number}.txt","w") as file:
        p = subprocess.Popen([x for x in f"/usr/bin/time -v {JAVA}{gc[1]} {set_memory[1]} {memory[1]} {pre_iteration_gc[2]}-Xlog:gc -jar {BENCHMARK} {benchmark} -n100000{pre_iteration_gc[1]}".split(" ") if x!="" and x!=None],stderr=file,stdout=file,cwd=f"./tmp-{number}/")
    return p

def percentile(list,percent):
    sorted_input = sorted(list)
    mid = len(sorted_input) * percent
    return sorted_input[math.floor(mid)] if math.ceil(mid)>=len(sorted_input) else (sorted_input[math.floor(mid)]+sorted_input[math.ceil(mid)])/2

def get_memory_info(list,prefix):
    list = sorted(list)
    tmp_avg = sum(list)/len(list)
    tmp_std = (sum([((x - tmp_avg) ** 2) for x in list]) / len(list))**.5
    return f"{prefix}_max:{max(list)}\n{prefix}_avg:{tmp_avg}\n{prefix}_std:{tmp_std}\n{prefix}_med:{percentile(list,.5)}\n{prefix}_90:{percentile(list,.9)}\n{prefix}_95:{percentile(list,.95)}\n{prefix}_99:{percentile(list,.99)}\n"


def input_thread_function():
    global PAUSE_MUTEX_LOCK
    while 1:
        while input()!="pause":
            pass
        PAUSE_MUTEX_LOCK.acquire()
        try:
            tqdm.tqdm.write("pausing - type \"continue\"  to continue")
        except:
            tqdm.tqdm.write("pausing - type \"continue\"  to continue")
        while input() !="continue":pass
        PAUSE_MUTEX_LOCK.release()


def main():
    global JAVA
    global JAVA_FLAGS
    global BENCHMARK
    global CGROUP
    global pids
    global threads

    atexit.register(enable_echo, True)
    enable_echo(False)


    starting_location=None
    if(len(sys.argv)>1):
        starting_location=sys.argv[1]


    with open("/sys/kernel/mm/transparent_hugepage/enabled","w") as file:
        file.write("never")
    if not os.path.isdir(f"/sys/fs/cgroup/{CGROUP}"):
        os.mkdir(f"/sys/fs/cgroup/{CGROUP}")
    with open(f"/sys/fs/cgroup/{CGROUP}/memory.max","w") as file:
        file.write(f"max");

    memory_names=None
    input_thread = threading.Thread(target=input_thread_function)
    input_thread.start()
    #pre_iteration_options=[("gc","",""),("no-gc"," --no-pre-iteration-gc",""),("disable-gc","","-XX:+DisableExplicitGC ")]
    pre_iteration_options=[("disable-gc","","-XX:+DisableExplicitGC ")]
    collectors=[("zgc"," -XX:+UseZGC"),("g1","")]
    #collectors=[("zgc"," -XX:+UseZGC")]
    #collectors=[("g1","")]
    #set_memory_options=[("default",""),("16 threads","-XX:ConcGCThreads=16 -XX:ParallelGCThreads=16")]
    set_memory_options=[("default","")]
    #benchmarks=["h2","jme","jython","luindex","lusearch","pmd","sunflow","xalan","zxing","fop"]
    benchmarks=["h2","jython","luindex","lusearch","pmd","sunflow","xalan","zxing","fop"]
    #benchmarks=["lusearch"]
    memory_configs=[("default","",""),("32","-Xmx32m","-Xms32m -Xmx32m"),("64","-Xmx64m","-Xms64m -Xmx64m"),("128","-Xmx128m","-Xms128m -Xmx128m"),("256","-Xmx256m","-Xms256m -Xmx256m"),("512","-Xmx512m","-Xms512m -Xmx512m"),("1024","-Xmx1024m","-Xms1024m -Xmx1024m"),("2048","-Xmx2048m","-Xms2048m -Xmx2048m"),("4096","-Xmx4096m","-Xms4096m -Xmx4096m"),("8192","-Xmx8192m","-Xms8192m -Xmx8192m")]
    #memory_configs=[("7","-XX:ZIdleFraction=7","-XX:ZIdleFraction=7"),("13","-XX:ZIdleFraction=13","-XX:ZIdleFraction=13"),("25","-XX:ZIdleFraction=25","-XX:ZIdleFraction=25"),("50","-XX:ZIdleFraction=50","-XX:ZIdleFraction=50"),("0","-XX:ZIdleFraction=0","-XX:ZIdleFraction=0"),("35","-XX:ZIdleFraction=35","-XX:ZIdleFraction=35"),("35","-XX:ZIdleFraction=45","-XX:ZIdleFraction=45")]
    #memory_configs=[("200","-XX:MaxGCPauseMillis=200 ","-XX:ZIdleFraction=7")]
    memory_measures=[]
    threads = []
    comb = itertools.product([0,1,2,3],benchmarks,[1,16],collectors,pre_iteration_options,set_memory_options,memory_configs)

    for iteration,benchmark,num_processes,collector,pre_iteration_gc,set_memory,memory_config in tqdm.tqdm([x for x in comb]):

        if (starting_location!=None):
            if(starting_location == "/".join([str(tmp_i) for tmp_i in[num_processes,collector[0],pre_iteration_gc[0],set_memory[0],benchmark,memory_config[0],iteration]])):
                starting_location=None
            continue;

        with open("/proc/sys/vm/drop_caches","w") as drop_cache:
            drop_cache.write("3")
        config_time = time.time()
        memory_measures=[]
        processes=list()

        directory = out_dir(benchmark,memory_config,collector,set_memory,iteration,pre_iteration_gc,num_processes)

        tqdm.tqdm.write(directory)



        with open(f"/sys/fs/cgroup/{CGROUP}/cgroup.procs","w") as file:
            for i in range(num_processes):
                tmp_pid=spawn_process(i,benchmark,memory_config,collector,set_memory,pre_iteration_gc,directory)
                file.write(f"{tmp_pid.pid}\n")
                file.flush()
                processes.append(tmp_pid)



        memory_measures=[]
        exit =0;
        stop=time.time()+(60*5)
        while time.time()<stop and exit==0:
            for i in processes:
                if i.poll()!=None:
                    exit=1
            with open(f"/sys/fs/cgroup/{CGROUP}/memory.stat","r") as file:
                tmp_memory_file_content=file.read()
                memory_measures.append(tuple([int(line.split(" ")[1]) for line in tmp_memory_file_content.split("\n") if len(line)>0]))
                if memory_names==None:
                    memory_names=[line.split(" ")[0] for line in tmp_memory_file_content.split("\n") if len(line)>=0]
            if exit==0:
                time.sleep(min(0.5,abs(stop-time.time())))
        if(exit==1):
            tqdm.tqdm.write("Failed to complete")
            with open(f"{directory}/fail.txt","w") as file:
                file.write("exited early")

        kill_processes(processes)
        old_time = time.time()
        with open(f"{directory}/memory-usage.txt","w+") as out_file:
            out_string=""
            for i in zip(memory_names,list(map(list, zip(*memory_measures)))):
                out_string += get_memory_info(i[1],i[0])
            out_file.write(out_string)
            out_file.flush()
        tqdm.tqdm.write(f"Time to print memory usage:{time.time()-old_time}")
        tqdm.tqdm.write(f"Total Time:{time.time()-config_time}")

        if not PAUSE_MUTEX_LOCK.acquire(blocking=False):
            tqdm.tqdm.write("The main thread is now paused")
            PAUSE_MUTEX_LOCK.acquire()
        PAUSE_MUTEX_LOCK.release()
    sys.exit()

if __name__=="__main__":
    os.setpgrp()
    main()
